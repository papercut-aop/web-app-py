FROM python:3.10-slim

# Allow statements and log messages to immediately appear in the Knative logs
ENV PYTHONUNBUFFERED True

COPY requirements.txt /requirements.txt

RUN pip install --no-cache-dir -r /requirements.txt && \
    useradd runner -d /app

WORKDIR /app
EXPOSE 8787

COPY templates/ ./templates/
COPY *.py ./
COPY aop_lib ./aop_lib
COPY conf/ ./conf/
RUN chown -R runner:runner . && chmod +x *.py

USER runner

CMD ["./add.py"]
