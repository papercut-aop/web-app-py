#!/usr/bin/env python

# A simple example of PaperCut cloud-native Add-nn solution

# Note this is not production code. It's designed to show what needs to be implemented in a simple manner
# Useful Links:

# https://palletsprojects.com/p/flask/
# https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https  (see also https://stackoverflow.com/questions/41568185/how-to-implement-tls-ssl-for-python-bottle-using-gevent)

from requests import post, codes
from flask import Flask, request, render_template
from logging import critical, info, debug, exception, basicConfig, DEBUG
from logging.config import dictConfig
from os import environ

from aop_lib.ourConfig import getConfig
from aop_lib.ourDatabase import getCustomer, storeCustomer, removeCustomer
from AOP_APIwrappers import verifyToken

basicConfig(level=DEBUG)

ourConfig = getConfig()  # get the basic connection info

# Flask setup
hostname = ourConfig['hostname']
port =  port=int(environ.get('PORT', ourConfig['port']))
app = Flask(__name__)


@app.route('/connect/<string:myProductId>/remove')
def remove(myProductId):
    app.logger.debug(f"request headers are {request.headers}")
    app.logger.debug(f"request url args are {request.args}")

    customerOrg = request.args.get("orgId")
    connectionToken = request.args.get("token")

    productId = request.args.get("product")
    productName = request.args.get("productName")

    #TODO verify language
    # customerLang = request.headers.get("Accept-Language").split(',',1)[0]
    
    app.logger.debug(f"myproductID is {myProductId}")
    ourConfig = getConfig(myProductId)

    app.logger.debug(f"Return from getConfig and product name is {ourConfig['product-id']}")

    if not myProductId == ourConfig['product-id']:
        critical(f"Unknown solution requested {myProductId}")
        return f"<h1>{myProductId} is no currently supported</h1>", 404

    if not verifyToken(connectionToken, customerOrg, myProductId):
        critical(f"Cannot validate token")
        return "<h1>Invalid request</h1>", 401

    app.logger.debug(f"Disconnect Request")
    removeCustomer(customerOrg, myProductId, ourConfig['firebase-info'])
    return "Connection Removed", 200



@app.route('/connect/<string:myProductId>/setup')
def connect(myProductId):
    
    app.logger.debug(f"request headers are {request.headers}")
    app.logger.debug(f"request url args are {request.args}")

    customerOrg = request.args.get("orgId")
    connectionToken = request.args.get("token")

    productId = request.args.get("product")
    productName = request.args.get("productName")

    #TODO verify language
    # customerLang = request.headers.get("Accept-Language").split(',',1)[0]
    
    app.logger.debug(f"myproductID is {myProductId}")
    ourConfig = getConfig(myProductId)

    app.logger.debug(f"Return from getConfig and product name is {ourConfig['product-id']}")

    if not myProductId == ourConfig['product-id']:
        critical(f"Unknown solution requested {myProductId}")
        return f"<h1>{myProductId} is no currently supported</h1>", 404

    if not verifyToken(connectionToken, customerOrg, myProductId):
        critical(f"Cannot validate token")
        return "<h1>Invalid request</h1>", 401


    app.logger.debug("Connection Request")

    #try and see if it's an existing customer
    customerDetails = getCustomer(customerOrg, ourConfig['firebase-info'])

    debug(f"getCustomer() returns {customerDetails}")

    if customerDetails is not None and customerDetails['myProductId'] == ourConfig['product-id']:
        return render_template( "no-config-required.jinja",
                                supplierName= ourConfig['supplier-name'], solutionName=ourConfig['solution-name'])

    return render_template( "acceptance.jinja",
                            solutionName=ourConfig['solution-name'],
                            supplierName=ourConfig['supplier-name'],
                            orgId=customerOrg,
                            connectionToken=connectionToken,
                            productId=productId,
                            productName=productName,
                            myProductId=myProductId,
                            customerName="Please tell us who you are")

@app.route("/acceptance", methods=['POST'])
def accept():

    debug("started accept()")
    if request.method != 'POST':
        app.logger.error(f"In Accept(), method is {request.method}, Should be POST")
        return None

    if request.form['acceptance'] == "no":
        app.logger.error(f"In Accept(), customer declined acceptance")
        return None

    connectionToken = request.form['connectionToken']
    customerOrg = request.form["orgId"]
    customerName = request.form["customerName"]
    productId = request.form["productId"]
    myProductId = request.form["myProductId"]
    productName = request.form["productName"]

    try:
        ourConfig = getConfig(myProductId)

    except:
        critical(f"Unknown solution requested {myProductId}")
        return f"<h1>{myProductId} is no currently supported</h1>", 404

    if not verifyToken(connectionToken, customerOrg, myProductId):
        critical(f"Cannot validate token")

    if request.form['acceptance'] != "yes":

        return render_template( "acceptance.jinja",
                            solutionName=ourConfig['solution-name'],
                            supplierName=ourConfig['supplier-name'],
                            orgId=customerOrg,
                            connectionToken=connectionToken,
                            productId=productId,
                            productName=productName,
                            myProductId=myProductId,
                            customerName="Please tell us who you are")

    app.logger.debug(f"form fields in acceptance are {request.form}")


    customerData =  {"org-name": customerName, "myProductId": myProductId}


    storeCustomer(customerOrg, customerData, ourConfig['firebase-info'])

    return "OK"


if __name__ == "__main__":
    app.run( host=hostname, port=port, debug=True)
