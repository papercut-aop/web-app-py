
$env:GCP_PROJECT_ID = & gcloud config get-value project

gcloud builds submit --tag gcr.io/${env:GCP_PROJECT_ID}/add-poc

gcloud run deploy  --image=gcr.io/${env:GCP_PROJECT_ID}/add-poc --platform=managed --region=us-central1 --allow-unauthenticated aop-poc
