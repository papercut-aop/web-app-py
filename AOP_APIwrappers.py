#!/usr/bin/env python

from requests import post, codes, get
from logging import debug, basicConfig, error, info, DEBUG, exception
from sys import exit

from aop_lib.ourConfig import getConfig
from aop_lib.manageAccessTokens import getAccessToken

basicConfig(level=DEBUG)

def verifyToken(token, org, productID):

    ourConfig = getConfig(productID)

    apiVersion = ourConfig.get('api-version')
    apiRoot = ourConfig.get('api-root')

    debug(f"verifyToken(): token = {token}, org = {org}")

    resp = post(f"{apiRoot}/{org}/addons.verify-options-token/{apiVersion}",
        headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Content-type":"application/json",
                "Accept": "application/json"},
        json={"token": token })

    debug(f"{resp.status_code}")

    return resp.status_code == codes.ok
