# PaperCut cloud-native Add-on platform API demo program

`add.py` -- supports requests from the PaperCut platform to add, configure and disconnect organisations

**NOTE**: This program uses submodules. Clone with the `--recurse-submodules` option. e.g.

```
git clone --recurse-submodules git@gitlab.com:papercut-aop/data-export-py.git
```

Platform docs are [here](https://papercut-aop.gitlab.io/api-docs/)

## `Add.py`

This web application needs to run on a public IP address and use tls (https)

A simple dockerfile is provided to create a container. This can be deployed to a service such
[GCP Cloud Run](https://cloud.google.com/run).

For demo purposes this code makes use of the [GCP Firebase](https://cloud.google.com/firestore)

Please note that GCP is used for demonstration purposes only. Any hosting or database technology can be used.

### Set up

1. Edit the file `conf/config.ini` and add the API key supplied by PaperCut
2. In the GCP Firebase console create a credential file and store it in `src/conf/key.json`

### Creating the Docker Image on CCP Cloud Run

1. Make sure that you have authenticated to GCP and set the correct GCP project with the `gcloud` utility
2. Run the `src/buil-adder.ps1` (this script is easy to convert for a POSIX shell)

### Running the container on GCP

1. In the GCP Cloud Run console set up a service and add your new container.
2. Take a note of the public URL and ask PaperCut to configure your test environment with that URL
